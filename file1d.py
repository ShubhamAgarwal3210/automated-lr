# state = ([['t'],[],['s','v','r','m','p','j','q']],[[],['1'],['2','3','4','5','6','7','8']],[[['t','','',''],['','','',''],['1','','',''],['','','','']],[['','','','t'],['','','',''],['1','','',''],['','','','']]])

import list_tuple_conversion

'''
Person(uv_kv_niv)           =   state[0]
Designation(uv_kv_niv)      =   state[1]
Cases                       =   state[2]

case0                       =   state[2][0]
case0PersonRow1             =   state[2][0][0]
case0PersonRow2             =   state[2][0][1]
case0PersonDesignationRow1  =   state[2][0][2]
case0PersonDesignationRow2  =   state[2][0][3]
'''

def remove_dublicate_case(valid_case):
    new_valid_case_list = []

    for i in valid_case:
        if new_valid_case_list.count(i) == 0:
            new_valid_case_list.append(i)
    return new_valid_case_list

def get_state_from_states(valid_case):
    for i in range(0,len(valid_case)):
        valid_case[i][0][0].sort()
        valid_case[i][0][1].sort()
        valid_case[i][0][2].sort()
        valid_case[i][1][0].sort()
        valid_case[i][1][1].sort()
        valid_case[i][1][2].sort()

    visited = []
    all_state = []
    for i in range(0,len(valid_case)):
        tmp_case = []
        uv_kv_niv = []
        uv_kv_niv.append(valid_case[i][0])
        uv_kv_niv.append(valid_case[i][1])

        if visited.count(valid_case[i][2]) == 0:
            visited.append(valid_case[i][2])
            tmp_case.append(valid_case[i][2])

        for j in range(0,len(valid_case)):
            uv_kv_niv2 = []
            uv_kv_niv2.append(valid_case[j][0])
            uv_kv_niv2.append(valid_case[j][1])
            if i != j and uv_kv_niv == uv_kv_niv2:
                if visited.count(valid_case[j][2]) == 0:
                    visited.append(valid_case[j][2])
                    tmp_case.append(valid_case[j][2])

        if len(tmp_case) > 0:
            uv_kv_niv.append(tmp_case)
            all_state = all_state + uv_kv_niv
    return all_state


def remove_from_lst(lst,x):
    temp_lst = []
    for i in lst:
        if i != x:
            temp_lst = temp_lst + list(i)
    return temp_lst

def add_to_lst(lst,x):
    return lst + list(x)


def generate_case_solver(state,id,person,designation):
    uvPerson = state[0][0]
    kvPerson = state[0][1]
    nivPerson = state[0][2]
    uvDesignation = state[1][0]
    kvDesignation = state[1][1]
    nivDesignation = state[1][2]
    personRow1 = state[2][id][0]
    personRow2 = state[2][id][1]
    designationRow1 = state[2][id][2]
    designationRow2 = state[2][id][3]

    if nivPerson.count(person) and kvDesignation.count(designation) and designationRow1.count(designation): #if person in kv and designation in uv and person in row1.
        position_of_designation = designationRow1.index(designation)
        if position_of_designation >= 0 and position_of_designation + 2 < 4 and  state[2][id][0][position_of_designation + 2] == '':
            state[2][id][0][position_of_designation + 2] = person

            kvPerson = add_to_lst(kvPerson,person)
            nivPerson = remove_from_lst(nivPerson,person)

            person_uv_kv_niv = [uvPerson,kvPerson,nivPerson]
            designation_uv_kv_niv = [uvDesignation,kvDesignation,nivDesignation]

            newstate = []
            newstate.append(person_uv_kv_niv)
            newstate.append(designation_uv_kv_niv)
            case = state[2][id]
            newstate.append(case)

            return newstate
    return []


def generate_case(state,id):
    list_of_tmp_state = []
    for person in ['s','v','r','m','p','j','q','t']:
        for designation in ['1','2','3','4','5','6','7','8']:
            tmp_state = generate_case_solver(state,id,person,designation)
            if len(tmp_state) != 0:
                list_of_tmp_state = list_of_tmp_state + tmp_state

    # tmp_state = generate_case_solver(state,id,'s','1')
    if len(tmp_state) != 0:
        list_of_tmp_state = list_of_tmp_state + tmp_state

    return list_of_tmp_state




def list_of_possible_actions(state):
    # state = list(state)
    valid_state = []
    valid_case = [] #will store all the valid and new case/cases after applying the action to the current state.
    for case in range(0,len(state[2])):
        action_applied_cases = generate_case(state,case)
        if len(action_applied_cases) > 0: #this means that the current case was valid and it has generated new case/cases after applying the action.
            valid_case.append(action_applied_cases) 

    valid_case = remove_dublicate_case(valid_case)
    if len(valid_case) > 0:
        valid_state = get_state_from_states(valid_case) #grouping cases which comes under a single state, and then returns list of all valid states.

    # for i in valid_state:
    #     print(i)
    return valid_state


def possible_actions(state):
    final_actions = []
    state_as_list = list_tuple_conversion.tuple_to_list(state)
    # state_as_list = state
    new_state_as_list = list_of_possible_actions(state_as_list)
    if len(new_state_as_list):
        final_actions.append(list_tuple_conversion.list_to_tuple(new_state_as_list))
    # print(final_actions)
    return final_actions



# print(possible_actions(state))