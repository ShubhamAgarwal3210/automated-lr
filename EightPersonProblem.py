from __future__ import print_function

from simpleai.search import astar, SearchProblem
from simpleai.search.viewers import WebViewer

import file1a,file1b,file1c,file1d,file1e,file1f,file1g,file1h
import file2a,file2b,file2c,file2d,file2e,file2f,file2g,file2h
import file4a,file4b,file4c,file4d,file4e,file4f,file4g,file4h
import file5a,file5b,file5c,file5d,file5e,file5f
import file6a,file6b,file6c,file6d,file6e,file6f,file6g,file6h

'''
Person(uv_kv_niv)           =   state[0]
Designation(uv_kv_niv)      =   state[1]
Cases                       =   state[2]
case0                       =   state[2][0]
case0PersonRow1             =   state[2][0][0]
case0PersonRow2             =   state[2][0][1]
case0PersonDesignationRow1  =   state[2][0][2]
case0PersonDesignationRow2  =   state[2][0][3]
'''
INITIAL = (((), (), ('s', 'v', 'r', 'm', 'p', 'j', 'q', 't')), ((), (), ('1', '2', '3', '4', '5', '6', '7', '8')), ((('', '', '', ''), ('', '', '', ''), ('', '', '', ''), ('', '', '', '')),))

GOAL = 4

# INITIAL = (((), ('s', 'v'), ('r', 'm', 'p', 'j', 'q', 't')), (('1',), (), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', '')), (('', '', 'v', 's'), ('', '', '', ''), ('', '1', '', ''), ('', '', '', '')), (('', '', 'v', 's'), ('', '', '', ''), ('', '', '1', ''), ('', '', '', '')), (('', '', 'v', 's'), ('', '', '', ''), ('', '', '', '1'), ('', '', '', ''))))
# GOAL =    (((), ('s', 'v'), ('j', 'm', 'p', 'q', 'r', 't')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('', '1', '', ''), ('', '', '', '')),))

# INITIAL = (((), ('s', 'v'), ('r', 'm', 'p', 'j', 'q', 't')), ((), (), ('1', '2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('', '', '', ''), ('', '', '', '')), (('', '', 'v', 's'), ('', '', '', ''), ('', '', '', ''), ('', '', '', ''))))
# GOAL =    (((), ('s', 'v'), ('j', 'm', 'p', 'q', 'r', 't')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('1', '1', '', ''), ('', '', '', '')),))

# INITIAL = ((('s', 'v'), (), ('r', 'm', 'p', 'j', 'q', 't')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', '')), (('', '', 's', 'v'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', ''))))
# GOAL =    ((('v',), ('s',), ('j', 'm', 'p', 'q', 'r', 't')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 's', 'v'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', '')),))

# INITIAL = ((('t',), (), ('s', 'v', 'r', 'm', 'p', 'j', 'q')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('t', '', '', ''), ('', '', '', ''), ('1', '', '', ''), ('', '', '', '')), (('', '', '', 't'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', ''))))
# GOAL =    ((('t',), ('s',), ('j', 'm', 'p', 'q', 'r', 'v')), ((), ('1',), ('2', '3', '4', '5', '6', '7', '8')), ((('t', '', 's', ''), ('', '', '', ''), ('1', '', '', ''), ('', '', '', '')), (('', '', 's', 't'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', ''))))

# INITIAL = ((('s', 'v'), (), ('r', 'm', 'p', 'j', 'q', 't')), (('1',), (), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('', '1', '', ''), ('', '', '', '')), (('', '', 's', 'v'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', ''))))
# GOAL =    ((('s', 'v'), (), ('r', 'm', 'p', 'j', 'q', 't')), (('1',), (), ('2', '3', '4', '5', '6', '7', '8')), ((('', '', 'v', 's'), ('', '', '', ''), ('', '1', '', ''), ('', '', '', '')), (('', '', 's', 'v'), ('', '', '', ''), ('1', '', '', ''), ('', '', '', ''))))


class EightPersonProblem(SearchProblem):
    def actions(self, state):
        actions = []

        if len(file1a.possible_actions(state)):
            actions = actions + file1a.possible_actions(state)
        if len(file1b.possible_actions(state)):
            actions = actions + file1b.possible_actions(state)
        if len(file1c.possible_actions(state)):
            actions = actions + file1c.possible_actions(state)
        if len(file1d.possible_actions(state)):
            actions = actions + file1d.possible_actions(state)
        if len(file1e.possible_actions(state)):
            actions = actions + file1e.possible_actions(state)
        if len(file1f.possible_actions(state)):
            actions = actions + file1f.possible_actions(state)
        if len(file1g.possible_actions(state)):
            actions = actions + file1g.possible_actions(state)
        if len(file1h.possible_actions(state)):
            actions = actions + file1h.possible_actions(state)

        if len(file2a.possible_actions(state)):
            actions = actions + file2a.possible_actions(state)
        if len(file2b.possible_actions(state)):
            actions = actions + file2b.possible_actions(state)
        if len(file2c.possible_actions(state)):
            actions = actions + file2c.possible_actions(state)
        if len(file2d.possible_actions(state)):
            actions = actions + file2d.possible_actions(state)
        if len(file2e.possible_actions(state)):
            actions = actions + file2e.possible_actions(state)
        if len(file2f.possible_actions(state)):
            actions = actions + file2f.possible_actions(state)
        if len(file2g.possible_actions(state)):
            actions = actions + file2g.possible_actions(state)
        if len(file2h.possible_actions(state)):
            actions = actions + file2h.possible_actions(state)

        if len(file4a.possible_actions(state)):
            actions = actions + file4a.possible_actions(state)
        if len(file4b.possible_actions(state)):
            actions = actions + file4b.possible_actions(state)
        if len(file4c.possible_actions(state)):
            actions = actions + file4c.possible_actions(state)
        if len(file4d.possible_actions(state)):
            actions = actions + file4d.possible_actions(state)
        if len(file4e.possible_actions(state)):
            actions = actions + file4e.possible_actions(state)
        if len(file4f.possible_actions(state)):
            actions = actions + file4f.possible_actions(state)
        if len(file4g.possible_actions(state)):
            actions = actions + file4g.possible_actions(state)
        if len(file4h.possible_actions(state)):
            actions = actions + file4h.possible_actions(state)

        if len(file5a.possible_actions(state)):
            actions = actions + file5a.possible_actions(state)
        if len(file5b.possible_actions(state)):
            actions = actions + file5b.possible_actions(state)
        if len(file5c.possible_actions(state)):
            actions = actions + file5c.possible_actions(state)
        if len(file5d.possible_actions(state)):
            actions = actions + file5d.possible_actions(state)
        if len(file5e.possible_actions(state)):
            actions = actions + file5e.possible_actions(state)
        if len(file5f.possible_actions(state)):
            actions = actions + file5f.possible_actions(state)

        if len(file6a.possible_actions(state)):
            actions = actions + file6a.possible_actions(state)
        if len(file6b.possible_actions(state)):
            actions = actions + file6b.possible_actions(state)
        if len(file6c.possible_actions(state)):
            actions = actions + file6c.possible_actions(state)
        if len(file6d.possible_actions(state)):
            actions = actions + file6d.possible_actions(state)
        if len(file6e.possible_actions(state)):
            actions = actions + file6e.possible_actions(state)
        if len(file6f.possible_actions(state)):
            actions = actions + file6f.possible_actions(state)
        if len(file6g.possible_actions(state)):
            actions = actions + file6g.possible_actions(state)
        if len(file6h.possible_actions(state)):
            actions = actions + file6h.possible_actions(state)


        return actions

    def result(self, state, action):
        return action

    def is_goal(self, state):
        print("Goal:",state)
        return state == GOAL

    def cost(self, state1, action, state2):
        return 1 

    def heuristic(self, state):
        '''Returns an *estimation* of the distance from a state to the goal.'''
        return 1


# result = astar(EightPersonProblem(INITIAL),viewer=WebViewer())
result = astar(EightPersonProblem(INITIAL))
print(result)

for action, state in result.path():
    print('Action : ', action)
    print(state)