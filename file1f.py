# state =([[],[],['s','v','r','m','p','j','q','t']],[[],[],['1','2','3','4','5','6','7','8']],[[['','','',''],['','','',''],['','','',''],['','','','']]])
state =([[],['t'],['s','v','r','m','p','j','q']],[[],['8'],['1','2','3','4','5','6','7']],[[['','','t',''],['','','',''],['8','','',''],['','','','']]])

import list_tuple_conversion


'''
Person(uv_kv_niv)           =   state[0]
Designation(uv_kv_niv)      =   state[1]
Cases                       =   state[2]

case0                       =   state[2][0]
case0PersonRow1             =   state[2][0][0]
case0PersonRow2             =   state[2][0][1]
case0PersonDesignationRow1  =   state[2][0][2]
case0PersonDesignationRow2  =   state[2][0][3]
'''

def remove_dublicate_case(valid_case):
    new_valid_case_list = []

    for i in valid_case:
        if new_valid_case_list.count(i) == 0:
            new_valid_case_list.append(i)
    return new_valid_case_list

def make_placement(lst,pos,char):
    lst[pos] = char
    return lst

def uv_to_kv(state,case,person,designation):
    uvPerson = state[0][0]
    kvPerson = state[0][1]
    nivPerson = state[0][2]
    uvDesignation = state[1][0]
    kvDesignation = state[1][1]
    nivDesignation = state[1][2]
    personRow1 = state[2][0][0]
    personRow2 = state[2][0][1]
    designationRow1 = state[2][0][2]
    designationRow2 = state[2][0][3]

    kvPerson = add_to_lst(kvPerson,person)
    kvDesignation = add_to_lst(kvDesignation,designation)

    uvPerson = remove_from_lst(uvPerson,person)
    uvDesignation = remove_from_lst(uvDesignation,designation)

    person_uv_kv_niv = [uvPerson,kvPerson,nivPerson]
    designation_uv_kv_niv = [uvDesignation,kvDesignation,nivDesignation]

    newstate = []
    newstate.append(person_uv_kv_niv)
    newstate.append(designation_uv_kv_niv)
    case = [state[2][0]]
    newstate.append(case)

    return newstate


def niv_to_kv(state,case,person,designation):
    uvPerson = state[0][0]
    kvPerson = state[0][1]
    nivPerson = state[0][2]
    uvDesignation = state[1][0]
    kvDesignation = state[1][1]
    nivDesignation = state[1][2]
    personRow1 = state[2][0][0]
    personRow2 = state[2][0][1]
    designationRow1 = state[2][0][2]
    designationRow2 = state[2][0][3]

    kvPerson = add_to_lst(kvPerson,person)
    kvDesignation = add_to_lst(kvDesignation,designation)

    nivPerson = remove_from_lst(nivPerson,person)
    nivDesignation = remove_from_lst(nivDesignation,designation)

    person_uv_kv_niv = [uvPerson,kvPerson,nivPerson]
    designation_uv_kv_niv = [uvDesignation,kvDesignation,nivDesignation]

    newstate = []
    newstate.append(person_uv_kv_niv)
    newstate.append(designation_uv_kv_niv)
    newstate.append([case])

    return newstate


def niv_to_uv(state,case,person,designation):
    uvPerson = state[0][0]
    kvPerson = state[0][1]
    nivPerson = state[0][2]
    uvDesignation = state[1][0]
    kvDesignation = state[1][1]
    nivDesignation = state[1][2]
    personRow1 = state[2][0][0]
    personRow2 = state[2][0][1]
    designationRow1 = state[2][0][2]
    designationRow2 = state[2][0][3]

    uvPerson = add_to_lst(uvPerson,person)
    uvDesignation = add_to_lst(uvDesignation,designation)

    nivPerson = remove_from_lst(nivPerson,person)
    nivDesignation = remove_from_lst(nivDesignation,designation)

    person_uv_kv_niv = [uvPerson,kvPerson,nivPerson]
    designation_uv_kv_niv = [uvDesignation,kvDesignation,nivDesignation]

    newstate = []
    newstate.append(person_uv_kv_niv)
    newstate.append(designation_uv_kv_niv)
    newstate.append(case)

    return newstate




def get_state_from_case(state,valid_case):
    newstate = []
    newstate.append(state[0])
    newstate.append(state[1])
    newstate.append(valid_case)
    

    return newstate


def remove_from_lst(lst,x):
    temp_lst = []
    for i in lst:
        if i != x:
            temp_lst = temp_lst + list(i)
    return temp_lst

def add_to_lst(lst,x):
    return lst + list(x)


def generate_case_solver(state,id,person,designation):
    uvPerson = state[0][0]
    kvPerson = state[0][1]
    nivPerson = state[0][2]
    uvDesignation = state[1][0]
    kvDesignation = state[1][1]
    nivDesignation = state[1][2]
    # personRow1 = state[2][id][0]
    personRow2 = state[2][id][1]
    # designationRow1 = state[2][id][2]
    designationRow2 = state[2][id][3]

    possible_cases = []
    if nivPerson.count(person) and nivDesignation.count(designation): #if person in kv and designation in uv and person in row1.
        
        for i in range(0,len(state[2][id][2])-2):
            if state[2][id][2][i] == '' and state[2][id][0][i+2] == '':
                personRow1 = state[2][id][0].copy()
                designationRow1 = state[2][id][2].copy()
                personRow1 = make_placement(personRow1,i+2,person)
                designationRow1 = make_placement(designationRow1,i,designation)
                case = [personRow1,personRow2,designationRow1,designationRow2]
                possible_cases.append(case)

    return possible_cases


def generate_case(state,id):
    list_of_tmp_state = []
    # for person in ['s','v','r','m','p','j','q','t']:
    #     for designation in ['1','2','3','4','5','6','7','8']:
    #         tmp_state = generate_case_solver(state,id,person,designation)
    #         if len(tmp_state) != 0:
    #             list_of_tmp_state = list_of_tmp_state + tmp_state

    tmp_state = generate_case_solver(state,id,'s','1')
    if len(tmp_state) != 0:
        list_of_tmp_state = list_of_tmp_state + tmp_state

    return list_of_tmp_state




def list_of_possible_actions(state):
    # state = list(state)
    valid_state = []
    valid_case = [] #will store all the valid and new case/cases after applying the action to the current state.
    for case in range(0,len(state[2])):
        action_applied_cases = generate_case(state,case)
        if len(action_applied_cases) > 0: #this means that the current case was valid and it has generated new case/cases after applying the action.
            # valid_case.append(action_applied_cases) 
            valid_case = valid_case + action_applied_cases

    valid_case = remove_dublicate_case(valid_case)
    if len(valid_case) == 1:
        valid_state = niv_to_kv(state,valid_case[0],'s','1')# if only one valid state is their so the person and degistion will be in kv
    elif len(valid_case) > 1:
        valid_state = niv_to_uv(state,valid_case,'s','1') #grouping cases which comes under a single state, and then returns list of all valid states.

    # print(valid_state)
    # for i in valid_state:
    #     print(i)
    return valid_state


def possible_actions(state):
    final_actions = []
    state_as_list = list_tuple_conversion.tuple_to_list(state)
    # state_as_list = state
    new_state_as_list = list_of_possible_actions(state_as_list)
    if len(new_state_as_list):
        final_actions.append(list_tuple_conversion.list_to_tuple(new_state_as_list))
    # print(final_actions)
    return final_actions


print(possible_actions(state))



'''
state =(
    [[],[],['s','v','r','m','p','j','q','t']],
    [[],[],['1','2','3','4','5','6','7','8']],
    [
        [['','','',''],['','','',''],['','','',''],['','','','']]
    ]
)


[
    [['s'], [], ['v', 'r', 'm', 'p', 'j', 'q', 't']], 
    [['1'], [], ['2', '3', '4', '5', '6', '7', '8']], 
    [
        [['', '', 's', ''], ['', '', '', ''], ['1', '', '', ''], ['', '', '', '']], 
        [['', '', '', 's'], ['', '', '', ''], ['', '1', '', ''], ['', '', '', '']]
    ]
]
--------------------------------------------------------------------------------------------------------------------
state =(
    [[],['t'],['s','v','r','m','p','j','q']],
    [[],['8'],['1','2','3','4','5','6','7']],
    [
        [['','','t',''],['','','',''],['8','','',''],['','','','']]
    ]
)


[
    [[], ['t', 's'], ['v', 'r', 'm', 'p', 'j', 'q']], 
    [[], ['8', '1'], ['2', '3', '4', '5', '6', '7']], 
    [
        [['', '', 't', 's'], ['', '', '', ''], ['8', '1', '', ''], ['', '', '', '']]
    ]
]
'''