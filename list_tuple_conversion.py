# state = ([[],[],['s','v','r','m','p','j','q','t']],[[],[],['1','2','3','4','5','6','7','8']],[[['','','',''],['','','',''],['','','',''],['','','','']]])

def tuple_to_list(state):
    person_uv = list(state[0][0])
    person_kv = list(state[0][1])
    person_niv = list(state[0][2])
    person_uv_kv_niv = [person_uv,person_kv,person_niv]

    degisnation_uv = list(state[1][0])
    degisnation_kv = list(state[1][1])
    degisnation_niv = list(state[1][2])
    degisnation_uv_kv_niv = [degisnation_uv,degisnation_kv,degisnation_niv]

    cases = state[2]
    list_of_cases_final = []
    for case in cases:
        tmp_case_person_row1 = list(case[0])
        tmp_case_person_row2 = list(case[1])
        tmp_case_degisnation_row1 = list(case[2])
        tmp_case_degisnation_row2 = list(case[3])
        list_of_cases = [tmp_case_person_row1,tmp_case_person_row2,tmp_case_degisnation_row1,tmp_case_degisnation_row2]
        list_of_cases_final.append(list_of_cases) 

    newstate = (person_uv_kv_niv,degisnation_uv_kv_niv,list_of_cases_final)
    return newstate


def list_to_tuple(state):
    person_uv = tuple(state[0][0])
    person_kv = tuple(state[0][1])
    person_niv = tuple(state[0][2])
    person_uv_kv_niv = (person_uv,person_kv,person_niv)

    degisnation_uv = tuple(state[1][0])
    degisnation_kv = tuple(state[1][1])
    degisnation_niv = tuple(state[1][2])
    degisnation_uv_kv_niv = (degisnation_uv,degisnation_kv,degisnation_niv)

    cases = state[2]
    list_of_cases_final = []
    for case in cases:
        tmp_case_person_row1 = tuple(case[0])
        tmp_case_person_row2 = tuple(case[1])
        tmp_case_degisnation_row1 = tuple(case[2])
        tmp_case_degisnation_row2 = tuple(case[3])
        list_of_cases = (tmp_case_person_row1,tmp_case_person_row2,tmp_case_degisnation_row1,tmp_case_degisnation_row2)
        list_of_cases_final.append(list_of_cases) 

    list_of_cases_final = tuple(list_of_cases_final)
    newstate = (person_uv_kv_niv,degisnation_uv_kv_niv,list_of_cases_final)
    return newstate

# print(tuple_to_list(state))
# print(list_to_tuple(state))